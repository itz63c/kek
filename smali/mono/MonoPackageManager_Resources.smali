.class public Lmono/MonoPackageManager_Resources;
.super Ljava/lang/Object;


# static fields
.field public static Assemblies:[Ljava/lang/String;

.field public static Dependencies:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 50

    const-string v0, "ArtemisCompanion.Android.dll"

    const-string v1, "Acr.UserDialogs.dll"

    const-string v2, "AndHUD.dll"

    const-string v3, "ArtemisCompanion.dll"

    const-string v4, "FormsViewGroup.dll"

    const-string v5, "Plugin.Clipboard.Abstractions.dll"

    const-string v6, "Plugin.Clipboard.dll"

    const-string v7, "Plugin.FilePicker.dll"

    const-string v8, "Plugin.Permissions.dll"

    const-string v9, "Xamarin.Android.Arch.Core.Common.dll"

    const-string v10, "Xamarin.Android.Arch.Core.Runtime.dll"

    const-string v11, "Xamarin.Android.Arch.Lifecycle.Common.dll"

    const-string v12, "Xamarin.Android.Arch.Lifecycle.LiveData.Core.dll"

    const-string v13, "Xamarin.Android.Arch.Lifecycle.LiveData.dll"

    const-string v14, "Xamarin.Android.Arch.Lifecycle.Runtime.dll"

    const-string v15, "Xamarin.Android.Arch.Lifecycle.ViewModel.dll"

    const-string v16, "Xamarin.Android.Support.Animated.Vector.Drawable.dll"

    const-string v17, "Xamarin.Android.Support.Annotations.dll"

    const-string v18, "Xamarin.Android.Support.AsyncLayoutInflater.dll"

    const-string v19, "Xamarin.Android.Support.Collections.dll"

    const-string v20, "Xamarin.Android.Support.Compat.dll"

    const-string v21, "Xamarin.Android.Support.CoordinaterLayout.dll"

    const-string v22, "Xamarin.Android.Support.Core.UI.dll"

    const-string v23, "Xamarin.Android.Support.Core.Utils.dll"

    const-string v24, "Xamarin.Android.Support.CursorAdapter.dll"

    const-string v25, "Xamarin.Android.Support.CustomTabs.dll"

    const-string v26, "Xamarin.Android.Support.CustomView.dll"

    const-string v27, "Xamarin.Android.Support.Design.dll"

    const-string v28, "Xamarin.Android.Support.DocumentFile.dll"

    const-string v29, "Xamarin.Android.Support.DrawerLayout.dll"

    const-string v30, "Xamarin.Android.Support.Fragment.dll"

    const-string v31, "Xamarin.Android.Support.Interpolator.dll"

    const-string v32, "Xamarin.Android.Support.Loader.dll"

    const-string v33, "Xamarin.Android.Support.LocalBroadcastManager.dll"

    const-string v34, "Xamarin.Android.Support.Media.Compat.dll"

    const-string v35, "Xamarin.Android.Support.Print.dll"

    const-string v36, "Xamarin.Android.Support.SlidingPaneLayout.dll"

    const-string v37, "Xamarin.Android.Support.SwipeRefreshLayout.dll"

    const-string v38, "Xamarin.Android.Support.Transition.dll"

    const-string v39, "Xamarin.Android.Support.v7.AppCompat.dll"

    const-string v40, "Xamarin.Android.Support.v7.CardView.dll"

    const-string v41, "Xamarin.Android.Support.v7.RecyclerView.dll"

    const-string v42, "Xamarin.Android.Support.Vector.Drawable.dll"

    const-string v43, "Xamarin.Android.Support.VersionedParcelable.dll"

    const-string v44, "Xamarin.Android.Support.ViewPager.dll"

    const-string v45, "Xamarin.Essentials.dll"

    const-string v46, "Xamarin.Forms.Core.dll"

    const-string v47, "Xamarin.Forms.Platform.Android.dll"

    const-string v48, "Xamarin.Forms.Platform.dll"

    const-string v49, "Xamarin.Forms.Xaml.dll"

    filled-new-array/range {v0 .. v49}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmono/MonoPackageManager_Resources;->Assemblies:[Ljava/lang/String;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lmono/MonoPackageManager_Resources;->Dependencies:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

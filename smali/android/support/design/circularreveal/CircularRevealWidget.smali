.class public interface abstract Landroid/support/design/circularreveal/CircularRevealWidget;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/support/design/circularreveal/CircularRevealHelper$Delegate;


# virtual methods
.method public abstract buildCircularRevealCache()V
.end method

.method public abstract destroyCircularRevealCache()V
.end method

.method public abstract draw(Landroid/graphics/Canvas;)V
.end method

.method public abstract getCircularRevealOverlayDrawable()Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getCircularRevealScrimColor()I
.end method

.method public abstract getRevealInfo()Landroid/support/design/circularreveal/CircularRevealWidget$RevealInfo;
.end method

.method public abstract isOpaque()Z
.end method

.method public abstract setCircularRevealOverlayDrawable(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setCircularRevealScrimColor(I)V
.end method

.method public abstract setRevealInfo(Landroid/support/design/circularreveal/CircularRevealWidget$RevealInfo;)V
.end method

.class public abstract Landroid/support/transition/TransitionPropagation;
.super Ljava/lang/Object;


# virtual methods
.method public abstract captureValues(Landroid/support/transition/TransitionValues;)V
.end method

.method public abstract getPropagationProperties()[Ljava/lang/String;
.end method

.method public abstract getStartDelay(Landroid/view/ViewGroup;Landroid/support/transition/Transition;Landroid/support/transition/TransitionValues;Landroid/support/transition/TransitionValues;)J
.end method

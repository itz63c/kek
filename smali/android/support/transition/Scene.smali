.class public abstract Landroid/support/transition/Scene;
.super Ljava/lang/Object;


# direct methods
.method static getCurrentScene(Landroid/view/View;)Landroid/support/transition/Scene;
    .locals 1

    sget v0, Landroid/support/transition/R$id;->transition_current_scene:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/support/transition/Scene;

    return-object p0
.end method

.method static setCurrentScene(Landroid/view/View;Landroid/support/transition/Scene;)V
    .locals 1

    sget v0, Landroid/support/transition/R$id;->transition_current_scene:I

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public abstract exit()V
.end method

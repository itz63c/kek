.class public interface abstract Landroid/support/transition/Transition$TransitionListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onTransitionCancel(Landroid/support/transition/Transition;)V
.end method

.method public abstract onTransitionEnd(Landroid/support/transition/Transition;)V
.end method

.method public abstract onTransitionPause(Landroid/support/transition/Transition;)V
.end method

.method public abstract onTransitionResume(Landroid/support/transition/Transition;)V
.end method

.method public abstract onTransitionStart(Landroid/support/transition/Transition;)V
.end method
